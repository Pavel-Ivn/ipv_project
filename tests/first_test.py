import pytest

def sub(num1: int, num2: int):
    return num1 * num2


def test_sub_with_negative_nums():
    assert sub(10, 10) == 101, f'Expected result: 101, current result: {sub(10, 10)}'


def test_sub_with_negative_nums2():
    assert sub(10, 10) == 10, f'Expected result: 10, current result: {sub(10, 10)}'